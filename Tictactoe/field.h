#ifndef FIELD_H
#define FIELD_H
#pragma once

class field
{
public:
    field();
    enum jogador{x,o,NINGJ,EMCURSJ,NADAJ};
    enum campo{x_c,o_c,NADA};
    enum vitoria{x_v,o_v,NING,EMCURS};
    vitoria checa();
    jogador checaJ();
    struct ponto{
        unsigned int x=0;
        unsigned int y=0;
    };
    bool joga(jogador jog, int xp, int yp);
    void jogaJ(jogador jog, int xp, int yp);
    jogador vez();
    campo **getcampo();
private:
    campo **cmp;
    jogador turno = o;

};

#endif // FIELD_H
