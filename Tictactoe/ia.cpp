#include "ia.h"
#include <vector>
#include <iostream>
#include <windows.h>

using namespace std;

IA::IA()
{

}


void IA::inicia(field &campo, field::jogador IAplayer){
    this->campo = campo;

    this->AIjogador = IAplayer;
    if(IAplayer == field::x){
        this->HUjogador = field::o;
    }else{
        this->HUjogador = field::x;
    }

}


void printacamp(field::campo **campo){
    cout << "  0 1 2" << endl;
    for (int var = 0; var < 3; ++var) {
        cout << var;
        cout << "|";
        for (int var2 = 0; var2 < 3; ++var2) {

            switch (campo[var][var2]) {
            case field::NADA:
                cout << " ";
                break;
            case field::x_c:
                cout << "X";
                break;
            case field::o_c:
                cout << "O";
                break;
            default:
                break;
            }

            cout << "|";
        }
        cout << endl;
    }
}

void IA::movimenta(){ //lauches the AI move action
     tw=0;
     tl=0;
     tt=0;
    std::cout << std::endl; //little message
    std::cout << "**Pensando**" << std::endl; //little message
    cmp = campo.getcampo();                   //gets the pointer** to the tictactoe field
    AiMove jogada = MelhorJogada(AIjogador);  //gets the best move for the AIplayer
    campo.joga(AIjogador,jogada.x,jogada.y);  //puts the best move on the field
    cout << tw <<"/"<< tl << "/"<<tt << endl;

}

AiMove IA::MelhorJogada(field::jogador jogador){ //bruteforce through the possible moves and searches for the best one

    field::jogador vic = campo.checaJ(); // checks if someone wins the game (final node)

    if(vic == AIjogador){                //if AIplayer wins
        tw++;
        return AiMove(1);             //add 15 points
    } else if(vic == jogador){           //if human wins
        tl++;
        return AiMove(-1);            //add -15 points
    } else if(vic == field::NINGJ){      //if its a tie (full board no winners)
        tt++;
        return AiMove(0);               //score 0
    }                                  //if none of those, the game is still in progress
    std::vector<AiMove> jogadas;        //vector for moves, something tells me that it should be static but it gives alot of bugs and nosense moves (or no moves at all)
    AiMove jogada;                      //move for this iteration
    AiMove other;


    for (int y = 0; y < 3; ++y) {       //for each of the board
        for (int x = 0; x < 3; ++x) {
            if(cmp[x][y] == field::NADA){  // if its an empty space
                jogada.x = x;              //gets the x,y of the empty space
                jogada.y = y;
                campo.jogaJ(jogador,jogada.x,jogada.y); //makes a move

//                SHORT tabKeyState = GetAsyncKeyState( VK_TAB );
//                if( ( 1 << 16 ) & tabKeyState )
//                {
//                   printacamp(cmp);           //prints it on screen
//                }


                if(jogador == AIjogador){  //if its max(AI) next is human
                    other = MelhorJogada(HUjogador);
                    if(other.end == false)
                        jogada.pontos = other.pontos;
                    else
                        jogada.pontos = 0;
                }else{                     //if its min(Human) next is AI
                    other = MelhorJogada(AIjogador);
                    if(other.end == false)
                        jogada.pontos = other.pontos;
                    else
                        jogada.pontos = 0;
                }

                jogadas.push_back(jogada); //store the score of the final node
                campo.jogaJ(field::NADAJ,jogada.x,jogada.y); // delete the move made for testings
            }
        }
    }

    int melhorMov = 0;                  // bestMove is 0
    if(jogador == AIjogador){           // if AIturn(max)
        int melhorPon = -2;       // best pontuation = -∞
        for (int var = 0; var < jogadas.size(); ++var) { //best move/points of the subbranches
            if(jogadas.at(var).pontos > melhorPon){
                melhorMov = var;
                melhorPon = jogadas.at(var).pontos;
            }
        }
    } else {                            //if human(min)
        int melhorPon = 2;        //best pontution = +∞
        for (int var = 0; var < jogadas.size(); ++var) { //best move/points of the subbranches
            if(jogadas.at(var).pontos < melhorPon){
                melhorMov = var;
                melhorPon = jogadas.at(var).pontos;
            }
        }
    }

    if(jogadas.size() == 0)
       return AiMove(true);

    return jogadas.at(melhorMov); // return best move

}


