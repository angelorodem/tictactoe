#pragma once
#ifndef IA_H
#define IA_H
#include "field.h"
#include <vector>
struct AiMove {
    AiMove() {};
    AiMove(int Pontos) : pontos(Pontos) {}
    AiMove(bool End) : end(End) {}
    int x;
    int y;
    int pontos;
    bool end = false;
};


class IA
{

public:
        IA();
        void inicia(field &camp, field::jogador IAplayer);
        void movimenta();
private:
        field campo;
        AiMove MelhorJogada(field::jogador HUjogador);
        field::campo **cmp;
        field::jogador HUjogador;
        field::jogador AIjogador;
        int pensamentos = 0;
        int tw=0;
        int tl=0;
        int tt=0;

};
#endif // IA_H
