#include <iostream>
#include <cstdio>
#include <field.h>
#include <ia.h>
#include <windows.h>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

void mensagem(int cor,string mensagem){
    HANDLE  hConsole;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, cor);
    cout << mensagem;
    SetConsoleTextAttribute(hConsole, 7);
}

inline void printacampo(field::campo **campo){
    std::stringstream sstm;
    mensagem(10," 0 1 2\n");
    for (int var = 0; var < 3; ++var) {

        mensagem(15,"|");
        for (int var2 = 0; var2 < 3; ++var2) {

            switch (campo[var][var2]) {
            case field::NADA:
                cout << " ";
                break;
            case field::x_c:
                mensagem(12,"X");
                break;
            case field::o_c:
                mensagem(14,"O");
                break;
            default:
                break;
            }

            mensagem(15,"|");

        }
        sstm << var << "\n";
        mensagem(10,sstm.str());
        sstm.str(std::string());
    }
}

int main()
{
    int wat=0;
    bool cont = true;
    do{
    field toe;
    field::campo **campo;
    unsigned int x,y;
    bool certo = false;
    campo = toe.getcampo();
    IA ia;
    ia.inicia(toe,field::o);
     printacampo(campo);
    while (toe.checa() == field::EMCURS) {

//printacampo(campo);

        certo = false;
        if(toe.vez() == toe.x){
            while(certo == false){

                cout << "Player X quem joga! (x,y): ";
                fflush(stdin);
                scanf("%d%d",&y,&x);



                certo = toe.joga(field::x,x,y);

            }
        } else{
            while(certo == false){
                cout << "Player O quem joga!\n";
                ia.movimenta();
                 printacampo(campo);
                //fflush(stdin);
                //scanf("%d%d",&y,&x);

              // certo = toe.joga(field::o,x,y);
               certo = true;

            }
        }
    }
    printacampo(campo);
    if (toe.checa() == toe.x_v) {
        cout << "Vitoria player X" << endl;

        cout << "De novo? 1.Sim 2.Nope\n";
        fflush(stdin);
        scanf("%d",&wat);
        if(wat==2)
            cont=false;
    }else if (toe.checa() == toe.o_v) {
        cout << "Vitoria player O" << endl;

        cout << "De novo? 1.Sim 2.Nope\n";
        fflush(stdin);
        scanf("%d",&wat);
        if(wat==2)
            cont=false;
    } else if (toe.checa() == toe.NING) {
        cout << "Empate" << endl;

        cout << "De novo? 1.Sim 2.Nope\n";
        fflush(stdin);
        scanf("%d",&wat);
        if(wat==2)
            cont=false;
    }}while(cont);

    return 0;
}

