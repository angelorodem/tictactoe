TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    field.cpp \
    ia.cpp

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS_RELEASE += -Ofast -std=c++11

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    field.h \
    ia.h

