#include "field.h"
#include <iostream>

using namespace std;
field::field()
{
    cmp = new campo*[3];
    for (int x = 0; x < 3; ++x) {
        cmp[x] = new campo[3];
        for (int y = 0; y < 3; ++y) {
            cmp[x][y] = NADA;
        }
    }
    //std::cout << "Campo iniciado" << std::endl;


}

field::vitoria field::checa(){

    for(int l = 0;l < 3;l++){ //linha
        if(cmp[l][0] != cmp[l][1])
            continue;

        if(cmp[l][1] != cmp[l][2]){
            continue;
        }

        switch(cmp[l][1]){
        case o_c:
            return o_v;
            break;
        case x_c:
            return x_v;
            break;
        default:
            break;
        }
    }

    for(int l = 0;l < 3;l++){ // coluna
        if(cmp[0][l] != cmp[1][l])
            continue;

        if(cmp[1][l] != cmp[2][l]){
            continue;
        }

        switch(cmp[1][l]){
        case o_c:
            return o_v;
            break;
        case x_c:
            return x_v;
            break;
        default:
            break;
        }
    }


    if(cmp[0][0] == cmp[1][1] && cmp[1][1] == cmp[2][2]){
        switch(cmp[1][1]){
        case o_c:
            return o_v;
            break;
        case x_c:
            return x_v;
            break;
        default:
            break;
        }
    }

    if(cmp[0][2] == cmp[1][1] && cmp[1][1] == cmp[2][0]){
        switch(cmp[1][1]){
        case o_c:
            return o_v;
            break;
        case x_c:
            return x_v;
            break;
        default:
            break;
        }
    }


    for (int var = 0; var < 3; ++var) {
        for (int var2 = 0; var2 < 3; ++var2) {
            if(cmp[var][var2] == NADA){
                return EMCURS;
            }
        }
    }

    return NING;



}

field::jogador field::checaJ(){

    for(int l = 0;l < 3;l++){ //linha
        if(cmp[l][0] != cmp[l][1] || cmp[l][1] == NADA)
            continue;

        if(cmp[l][1] != cmp[l][2]){
            continue;
        }

        switch(cmp[l][1]){
        case o_c:
            return o;
            break;
        case x_c:
            return x;
            break;
        default:
            cout << "ERRO*************" << cmp[l][1] << endl;
            break;
        }
    }

    for(int l = 0;l < 3;l++){ // coluna
        if(cmp[0][l] != cmp[1][l] || cmp[1][l] == NADA)
            continue;

        if(cmp[1][l] != cmp[2][l]){
            continue;
        }

        switch(cmp[1][l]){
        case o_c:
            return o;
            break;
        case x_c:
            return x;
            break;
        default:
            cout << "ERRO*************" << cmp[1][l] <<endl;
            break;
        }
    }


    if(cmp[0][0] == cmp[1][1] && cmp[1][1] == cmp[2][2] && !(cmp[1][1] == NADA)){
        switch(cmp[1][1]){
        case o_c:
            return o;
            break;
        case x_c:
            return x;
            break;
        default:
            cout << "ERRO*************" <<cmp[1][1] << endl;
            break;
        }
    }

    if(cmp[0][2] == cmp[1][1] && cmp[1][1] == cmp[2][0] && !(cmp[1][1] == NADA)){
        switch(cmp[1][1]){
        case o_c:
            return o;
            break;
        case x_c:
            return x;
            break;
        default:
            cout << "ERRO*************" << cmp[1][1] <<endl;
            break;
        }
    }


    for (int var = 0; var < 3; ++var) {
        for (int var2 = 0; var2 < 3; ++var2) {
            if(cmp[var][var2] == NADA){
                return EMCURSJ;
            }
        }
    }

    return NINGJ;



}


field::campo **field::getcampo(){
    return cmp;
}

bool field::joga(field::jogador jog, int xp, int yp){
    if(xp >= 3 || xp < 0 )
        return false;

    if(yp >= 3 || yp < 0 )
        return false;

    if(cmp[xp][yp] == x_c || cmp[xp][yp] == o_c)
        return false;

    if (jog == field::x) {
        cmp[xp][yp] = x_c;
    }else if(jog == field::o) {
        cmp[xp][yp] = o_c;
    }
}


void field::jogaJ(field::jogador jog, int xp, int yp){
    if (jog == field::x) {
        cmp[xp][yp] = x_c;
        return;
    }else if(jog == field::o) {
        cmp[xp][yp] = o_c;
        return;
    }
        cmp[xp][yp] = NADA;

}

field::jogador field::vez(){
    if (turno == o) {
        turno = x;
        return x;
    } else if (turno == x) {
        turno = o;
        return o;
    }
}



